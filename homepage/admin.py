from copy import deepcopy
from django.contrib import admin
from mezzanine.pages.admin import PageAdmin
from mezzanine.core.admin import TabularDynamicInlineAdmin
from .models import HomePage, Slide, IconBlurb

class SlideInline(TabularDynamicInlineAdmin):
    model = Slide

class IconBlurbline(TabularDynamicInlineAdmin):
    model = IconBlurb

class HomePageAdmin(PageAdmin):
    inlines = (SlideInline, IconBlurbline)

admin.site.register(HomePage, HomePageAdmin)

