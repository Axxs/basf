from modeltranslation.translator import translator, TranslationOptions
from .models import HomePage, Slide, IconBlurb

class TranslatedHomePage(TranslationOptions):
    fields = ('heading','subheading','featured_works_heading','content_heading','latest_posts_heading',)

class TranslatedSlide(TranslationOptions):
    fields = ('caption',)

class TranslatedIconBlurb(TranslationOptions):
    fields = ('title','content',)

translator.register(HomePage, TranslatedHomePage)
translator.register(Slide, TranslatedSlide)
translator.register(IconBlurb, TranslatedIconBlurb)
