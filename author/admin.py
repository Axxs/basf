from copy import deepcopy
from django.contrib import admin
from mezzanine.pages.admin import PageAdmin
from .models import Author, Book

book_extra_fieldsets = ((None, {"fields": ("booksauthor","publication_date","cover","content")}),)

class BookAdmin(PageAdmin):
	fieldsets = deepcopy(PageAdmin.fieldsets) + book_extra_fieldsets

admin.site.register(Author, PageAdmin)
admin.site.register(Book, BookAdmin)

