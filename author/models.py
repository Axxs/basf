from django.db import models
from django.utils import timezone
from mezzanine.pages.models import Page
from mezzanine.core.models import RichText
from mezzanine.utils.models import upload_to

class Author(Page,RichText):
    '''
    A page representing an author 
    '''
    date_of_birth = models.DateField('Born', null=True, blank=True)
    date_of_death = models.DateField('Died', null=True, blank=True)
    picture = models.ImageField(upload_to="authors", blank=True)

    class Meta:
        verbose_name = 'author'
        verbose_name_plural = 'authors'

class Book(Page,RichText):
    '''
    A page representing the a book, connected to an author
    '''
    booksauthor = models.ForeignKey(Author, related_name="books", on_delete=models.CASCADE, null=True, blank=True )
    publication_date = models.DateField()
    cover = models.ImageField(upload_to="books", blank=True)

