from modeltranslation.translator import translator, TranslationOptions
from .models import Author, Book

class TranslatedAuthor(TranslationOptions):
    pass

class TranslatedBook(TranslationOptions):
    pass

translator.register(Author, TranslatedAuthor)
translator.register(Book, TranslatedBook)
